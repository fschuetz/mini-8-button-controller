# Mini 8 Button Controller
An 8 button controller with hardware debounced switches using a 74HC165 shift 
register to serialise the output. 

![Controller](./images/assembly.jpg)

## Getting started
This project contains all KiCad files needed to either order the breakout board
or adjust it as you see fit. For assembly you do not need to populate R17, R18, R19
and R20, unless you want to pullup / -down those pins. After soldering all the 
parts you can use the software provided to verify the correct operation of the 
controller.

The software currently is available for ESP32 and ESP32-C3 SOCs. Connect the 
controller to the ESP32 / ESP32-C3 as follows:
| Controller pin | ESP32 pin | ESP32-C3 pin | Description               |
| -------------- | --------- | ------------ | -------------             |
| SA             | GPIO5     | GPIO0        | Data line                 |
| CLK            | GPIO18    | GPIO1        | Clock line                |
| PL             | GPIO19    | GPIO2        | Parallel load             |
| CLE            | GPIO21    | GPIO3        | Clock enable (active low) |
| VCC            | VIN       | 3V3          | 5V or 3.3V rail           |
| GND            | GND       | GND          | Ground rail               |

Then change into the directory of the correct software matching you SOC and
execute:
```
idf.py flash monitor
```

Note: You need to have set up your esp-idf framework correctly. Check
[Getting started (ESP32)](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html)
or [Getting started (ESP32-C3)](https://docs.espressif.com/projects/esp-idf/en/latest/esp32c3/get-started/index.html)
if you haven't done so yet. Also keep in mind, that you only need the 
framework once as it supports both chips (and more). The instructions for
the different SOC version are only provided as a convenience, they are 
practically the same.

## Operation
There are eight buttons that input in parallel to a shift register. The shift register can then be read in serial by sending a clock signal to the controller. Each button is pulled up when not pressed and connected to ground when pressed. However, the shift register outputs the negated value, a button press will be indicated as a high signal. 

Further, each button is debounced in hardware using a capacitor.

## Parts
| Quantity | Item                         | Footprint         | LCSC Part |
| -------- | ---------------------------- | ----------------- | --------- |
| 1        | CD74HC165M96 shift register  | SOIC-16           | C470977   |
| 8        | XUNPU TS-1087S-02326 buttons | SMD,5.2x5.2x2.3mm | C455268   |
| 16       | 10k resistor                 | 0805              | C844937   |
| 8        | 100nF capacitor              | 0805              | C495959   |
| 1        | 2.45mm 1x5 pin header        | 2.45mm            | -         |

![Board](./images/board.jpg)

![Connected](./images/connected.jpg)

/* ESP32-C3 Button Input Test 

   This is a small test programm to verify the correct functioning of the mini
   8 button controller circuit using an ESP32-C3 SOC as the host that receives
   the button pushes.

   Make sure to connect the controller as follows to the ESP32-C3:
   	
   	Controller PIN 		ESP32-C3 PIN		Description 
	SA 			GPIO0 			Data line
	CLK 			GPIO1 			Clock signal
	PL 			GPIO2 			Parallel load 
	CLE 			GPIO3 			Clock enable 
	VCC 			3V3 			3.3V rail
	GND			GND 			Ground rail

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

#define DATA_PIN		0	
#define CLOCK_PIN		1
#define LOAD_PIN		2
#define CLOCK_ENABLE_PIN	3
#define GPIO_OUTPUT_PIN_SEL	((1ULL<<CLOCK_ENABLE_PIN) | (1ULL<<CLOCK_PIN) | (1ULL<<LOAD_PIN)) 
#define GPIO_INPUT_PIN_SEL	(1ULL<<DATA_PIN)

void app_main(void)
{
    // Configure output pins (CLE, CLK, PL)
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    // Configure input pin (SA). 
    // Reuse parameters from above.
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    io_conf.mode = GPIO_MODE_INPUT;
    gpio_config(&io_conf);

    // Initialise the value of the button pushes with 0
    int value = 0;
 
    // Prepare the clock by disabling it (avoid shift) and then 
    // setting the clock pin to high
    gpio_set_level(CLOCK_ENABLE_PIN, 1);	
    vTaskDelay(5 / portTICK_PERIOD_MS);
    gpio_set_level(CLOCK_PIN, 1);

    // Gather button pushes and display them. Repeat forever.
    while(true) {
	int dataIn = 0;

	// Load button state in register
	gpio_set_level(LOAD_PIN, 0);
	gpio_set_level(LOAD_PIN, 1);

	// Enable the clock and prepare a cycle by setting the clock pin 
	// to low. Note: The clock is active low.
	gpio_set_level(CLOCK_ENABLE_PIN, 0);	
	gpio_set_level(CLOCK_PIN, 0);

	// Query each button by cycling through all of the values in the 
	// shift register. After reading out a button value, the next one
	// is loaded by pulsing the clock from low to high (and then reset
	// to low to prepare next run)
	for(int i = 0; i < 8; i++) {
	   value = gpio_get_level(DATA_PIN);

	   if(value) {
	       int a = (1<<i);
	       dataIn = dataIn | a;
	   }
	   gpio_set_level(CLOCK_PIN, 1); 
	   gpio_set_level(CLOCK_PIN, 0);
	}

	// Disable clock
	gpio_set_level(CLOCK_ENABLE_PIN, 1);

	// Print the button pushes as binary number and in decimal.
	// Briefly pause before going through next cycle.
	printf("Button states are:\t");
	unsigned i;
	for (i = 1 << 7; i > 0; i = i / 2)
	    (dataIn & i) ? printf("1") : printf("0");
	printf("\t(%d)\n", dataIn);
	vTaskDelay(10 / portTICK_PERIOD_MS);
    }
}
